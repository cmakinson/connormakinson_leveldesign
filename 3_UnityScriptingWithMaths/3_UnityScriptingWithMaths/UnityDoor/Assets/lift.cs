﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lift : MonoBehaviour
{
    public Animation anim;
    public TriggerListener_Com trigger;
    public Image cursorImage;

    private void Start()
    {
        anim = GetComponent<Animation>();
        anim.Stop();
        cursorImage.enabled = false;
    }
    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseExit()
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseDown()
    {
        Debug.Log("animplay");
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {
            
            anim.Stop();
            anim.Play(); 
        }
    }

    private void Update()
    {
     
    }
}













    
