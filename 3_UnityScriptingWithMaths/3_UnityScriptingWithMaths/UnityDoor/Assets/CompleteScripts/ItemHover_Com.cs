﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHover_Com : MonoBehaviour
{
    public float moveAmount = 1f;
    public float moveSpeed = 1f;
    public float rotSpeed = 1f;
    Vector3 startPos;
    
    // Use this for initialization
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(startPos.x, startPos.y + (Mathf.Sin(Time.time * moveSpeed) * moveAmount), startPos.z);
        transform.rotation = Quaternion.Euler(new Vector3(0f, (Time.time * rotSpeed), 0f));
    }
}
