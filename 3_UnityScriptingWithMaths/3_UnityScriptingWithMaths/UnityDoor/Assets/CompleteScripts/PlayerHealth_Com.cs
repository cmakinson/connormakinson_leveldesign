﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth_Com : MonoBehaviour
{
    public float rechargeSpeed = 2f;
    float currentHealth = 1f;
    public Slider healthSlider;
    public AudioSource playerSound;
    public AudioSource rechargeSound;
    
    //damage player
    public void TakeDamage()
    {
        StopAllCoroutines();
        rechargeSound.Stop();
        InvokeRepeating("Damage", 0f, 1f);
    }

    public void StopDamage()
    {
        CancelInvoke();
        StartCoroutine("HealthRecharge");
    }

    void Damage()
    {
        currentHealth = currentHealth - 0.1f;
        playerSound.pitch = Random.Range(0.95f, 1.05f);
        playerSound.volume = Random.Range(0.8f, 1.2f);
        playerSound.Play();
        healthSlider.value = currentHealth;
        DeathCheck();
    }

    //recaharge health
    IEnumerator HealthRecharge()
    {
        yield return new WaitForSeconds(2f);
        rechargeSound.Stop();
        rechargeSound.Play();
        while (currentHealth < 0.995f)
        {
            currentHealth = Mathf.MoveTowards(currentHealth, 1f, Time.deltaTime * rechargeSpeed);
            healthSlider.value = currentHealth;
            yield return null;
        }
        currentHealth = 1f;
        healthSlider.value = currentHealth;
        yield return null;
    }
    
    void DeathCheck()
    {
        if (currentHealth <= 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}
