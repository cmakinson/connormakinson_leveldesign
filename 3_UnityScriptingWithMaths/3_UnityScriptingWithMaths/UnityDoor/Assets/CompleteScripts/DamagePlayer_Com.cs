﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer_Com : MonoBehaviour
{ 
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.BroadcastMessage("TakeDamage");
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.BroadcastMessage("StopDamage");
        }
    }
}
