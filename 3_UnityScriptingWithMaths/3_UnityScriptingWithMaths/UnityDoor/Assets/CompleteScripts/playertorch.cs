﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playertorch : MonoBehaviour {
    GameObject FPSController;
    GameObject torchobj;
    bool torchpickup;
    public MouseListen mouse;
    public Image cursorImage;
    bool inTrigger;
    bool clickHappened;
    

    // Use this for initialization
    void Start () {
        FPSController = GameObject.FindGameObjectWithTag("Player");
        cursorImage.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (inTrigger)
        {
            if (mouse.mouseCursorOn == true)
            {
                if (cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;
                }
            }
            else
            {
                cursorImage.enabled = false;
            }
        }

        if (inTrigger && mouse.mouseClicked && clickHappened == true)
        {
            Destroy(gameObject, 0f);
        }

        if (inTrigger && mouse.mouseClicked && clickHappened == false) {
            clickHappened = true;
            TorchobjInteract();
        }
    }

    private void TorchobjInteract()
    {
        throw new NotImplementedException();
    }

    void OnTriggerEnter(Collider col )
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
            Debug.Log("col works");
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = false;
            cursorImage.enabled = false;
        }
    }
    
}
