﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateDoor_Com : MonoBehaviour
{
    public GameObject door;
    public MouseListener_Com mouseListen;
    public float targetRot = 90f;
    GameObject myPlayer;
    bool doorOpen;
    bool inTrigger;
    bool clickHappened = false;
    public float moveSpeed = 1f;
    public float snapTo = 0.1f;
    public Image cursorImage;

    Vector3 closedRot;

    // Use this for initialization
    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = transform.localRotation.eulerAngles;
    }

    void Update()
    {
        if (inTrigger)
        {
            //turn on cursor when enters switch trigger with cursor over switch  
            if (mouseListen.mouseCursorOn == true)
            {
                if (cursorImage.enabled != true)
                {
                    cursorImage.enabled = true;
                }
            }
            //turn off cursor when player leaves trigger with cursor over switch  
            else
            {
                cursorImage.enabled = false;
            }
        }

        if (inTrigger && mouseListen.mouseClicked && !clickHappened)
        {
            clickHappened = true;
            DoorInteract();
        }
        else if(!mouseListen.mouseClicked)
        {
            clickHappened = false;
        }
    }
    
    //Works out which way door should open and starts coroutine
    void DoorInteract()
    {
        Vector3 finishPos;
        if (!doorOpen)
        {
            Vector3 playerDir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDir, transform.forward);
            Debug.Log(dot);
            doorOpen = true;

            if (dot > 0f)
            {
                finishPos = new Vector3(closedRot.x, closedRot.y + targetRot, closedRot.z);
            }
            else
            {
                finishPos = new Vector3(closedRot.x, closedRot.y - targetRot, closedRot.z);
            }
        }
        else
        {
            finishPos = closedRot;
            doorOpen = false;
        }
        StopCoroutine("DoorMotionCo");
        StartCoroutine("DoorMotionCo", finishPos);
}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = false;
            inTrigger = false;
        }
    }

    //Moves door
    IEnumerator DoorMotionCo(Vector3 target)
    {
        Debug.Log("Door Happening");
        while (Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) >= snapTo)
        {
            Debug.Log("Door Running");
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), moveSpeed * Time.deltaTime);
            
            yield return null;
        }
        clickHappened = false;
        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;
    }
}
