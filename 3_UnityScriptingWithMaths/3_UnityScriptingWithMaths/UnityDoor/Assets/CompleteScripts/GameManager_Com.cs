﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager_Com : MonoBehaviour
{
    public Text narrativeText;

	// Use this for initialization
	void Start ()
    {
        Invoke("IntroText1", 3);
	}

    void IntroText1()
    {
        narrativeText.text = "The year is 2235";
        Invoke("IntroText2", 3);
    }

    void IntroText2()
    {
        narrativeText.text = "The world is ruled by evil corporations";
        Invoke("IntroText3", 3);
    }

    void IntroText3()
    {
        narrativeText.text = "and everything is neon";
        Invoke("BlankText", 3);
    }
    void BlankText()
    {
        narrativeText.text = null;
    }
}
