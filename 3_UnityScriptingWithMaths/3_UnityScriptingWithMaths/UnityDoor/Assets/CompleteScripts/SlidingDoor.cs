﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float moveAmount = 1f;
    public float snap = 0.02f;
    public float speed = 5f; 

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    //Open Door
    void OnTriggerEnter(Collider col)
    {
     if(col.gameObject.tag == "Player")
        {
            
            //doorTran.localPosition = new Vector3(moveAmount, 0, 0);
            //doorTran.localPosition = new Vector(-moveAmount, 0, 0);
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", moveAmount);
        }
    }
    //Close Door
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
           
            //door1Tran.localPosition = Vector3.zero;
            //door2Tran.localPosition = Vector3.zero;
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    IEnumerator DoorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        
        while (xPos < (target - snap) || xPos > (target + snap))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
           
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
       
        yield return null;
    }
    
}
