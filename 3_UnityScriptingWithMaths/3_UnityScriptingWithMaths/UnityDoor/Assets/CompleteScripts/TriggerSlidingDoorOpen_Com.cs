﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSlidingDoorOpen_Com : MonoBehaviour
{
    AudioSource audioSource;

    public float doorOpenAmount = 1f;
    public Transform Door1Tran;
    public Transform Door2Tran;
    public float speed = 5f;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            StopCoroutine("DoorClose");
            StopCoroutine("DoorOpen");
            StartCoroutine("DoorOpen");
        }
    }

    void OnTriggerExit(Collider col)
    {        
        if (col.tag == "Player")
        {
            StopCoroutine("DoorClose");
            StopCoroutine("DoorOpen");
            StartCoroutine("DoorClose");
        }
    }

    IEnumerator DoorOpen()
    {
        audioSource.Play();
        float xPos = Door1Tran.localPosition.x;
        while (xPos < 0.98f)
        {
            xPos = Mathf.Lerp(Door1Tran.localPosition.x, doorOpenAmount, Time.deltaTime * speed);
            Door1Tran.localPosition = new Vector3(xPos, 0, 0);
            Door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Opening");
            yield return null;
        }
        Door1Tran.localPosition = new Vector3(doorOpenAmount, 0, 0);
        Door2Tran.localPosition = new Vector3(-doorOpenAmount, 0, 0);
        Debug.Log("Open");
        yield return null;
    }

    IEnumerator DoorClose()
    {
        audioSource.Play();
        float xPos = Door1Tran.localPosition.x;
        while (xPos > 0.02f)
        {
            xPos = Mathf.Lerp(Door1Tran.localPosition.x, 0, Time.deltaTime * speed);
            Door1Tran.localPosition = new Vector3(xPos, 0, 0);
            Door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Closing");
            yield return null;
        }
        Door1Tran.localPosition = Vector3.zero;
        Door2Tran.localPosition = Vector3.zero;
        Debug.Log("Closed");
        yield return null;
    }
}
