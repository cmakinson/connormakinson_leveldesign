﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseListener_Com : MonoBehaviour
{
    public bool mouseCursorOn;
    public bool mouseClicked;

    void OnMouseOver()
    {
        //turn on cursor when enters switch trigger with cursor over switch  
       
        if (mouseCursorOn != true)
        {
            mouseCursorOn = true;
        }
      
    }

    void OnMouseExit()
    {
        //turn off cursor when exits switch      
      
        mouseCursorOn = false;
        
    }

    void OnMouseDown()
    {
        mouseClicked = true;
    }

    void OnMouseUp()
    {
        mouseClicked = false;
    }
}
