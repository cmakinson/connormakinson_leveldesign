﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPickUp_Com : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Text Image;
    MeshRenderer meshRend;
    public bool keyPickedUp = false;
    

	// Use this for initialization
	void Start ()
    {
        meshRend = GetComponent<MeshRenderer>();
	}

    void OnMouseOver()
    {
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseExit()
    
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {
            meshRend.enabled = false;
            keyPickedUp = true;
            Invoke("KeyAquiredText", 0.02f);
            Invoke("BlankText", 2f);
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}

