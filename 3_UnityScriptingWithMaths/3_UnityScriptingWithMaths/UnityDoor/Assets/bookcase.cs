﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bookcase : MonoBehaviour
{

    public Transform farEnd;
    private Vector3 frometh;
    private Vector3 untoeth;
    private float secondsForOneLength = 10f;
    GameObject myPlayer;
    public Image cursorImage;
    public MouseListener_Com mouseListen;
    bool clickHappened = false;
    private bool inTrigger;
    

    private void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (inTrigger)
        {
            if (mouseListen.mouseCursorOn == true)
            {
                if (cursorImage.enabled != true)
                {
                    cursorImage.enabled = true;
                }
            }
            else
            {
                cursorImage.enabled = false;
            }
        }
        if (inTrigger && mouseListen.mouseClicked && !clickHappened)
        {
            clickHappened = true;
             transform.position = Vector3.Lerp(frometh, untoeth,
             Mathf.SmoothStep(0f, 1f,
             Mathf.PingPong(Time.time / secondsForOneLength, 1f)
             ));
        }
        else if (!mouseListen.mouseClicked)
        {
            clickHappened = false;
        }
      
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = false;
            inTrigger = false;
        }
    }


}
