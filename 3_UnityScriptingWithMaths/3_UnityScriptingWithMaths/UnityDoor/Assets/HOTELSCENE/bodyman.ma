//Maya ASCII 2018 scene
//Name: bodyman.ma
//Last modified: Tue, Jan 15, 2019 01:03:35 PM
//Codeset: 1252
requires maya "2018";
requires "mtoa" "3.1.0";
requires "mtoa" "3.1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 , 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -n "pCube1";
	rename -uid "C844E050-4CC6-6584-B103-EFBCD8A582A0";
	setAttr ".t" -type "double3" 0 1.8385590966985761 -3.7381072720150179 ;
	setAttr ".s" -type "double3" 1.1006372091180974 1 1.576247269613865 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "5801908C-4369-145B-7300-C68BB509927B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.4999995082616806 0.40625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 84 ".pt[884:967]" -type "float3"  -0.014412768 -0.020450994 
		1.6653345e-16 0.018449375 -0.020450994 1.6653345e-16 0.018449375 -0.020450994 1.6653345e-16 
		-0.014412768 -0.020450994 1.6653345e-16 -0.014412774 -0.020450994 1.3877788e-16 0.01844937 
		-0.020450994 1.3877788e-16 -0.014412768 -0.020450994 1.405126e-16 0.01844937 -0.020450994 
		1.405126e-16 -0.014412768 -0.020450994 1.6653345e-16 0.01844937 -0.020450994 1.6653345e-16 
		0.01844937 -0.020450994 1.110223e-16 -0.014412774 -0.020450994 1.110223e-16 -0.014412768 
		-0.020450994 1.110223e-16 0.01844937 -0.020450994 1.110223e-16 0.01844937 -0.020450994 
		1.6653345e-16 -0.014412768 -0.020450994 1.6653345e-16 0.01844937 -0.020450994 1.6653345e-16 
		-0.014412768 -0.020450994 1.6653345e-16 0.01844937 -0.020450994 1.6653345e-16 -0.014412774 
		-0.020450994 1.6653345e-16 0.01844937 -0.020450994 1.110223e-16 -0.014412768 -0.020450994 
		1.110223e-16 0.01844937 -0.020450994 1.110223e-16 -0.014412768 -0.020450994 1.110223e-16 
		0.01844937 -0.020450994 1.110223e-16 -0.014412768 -0.020450994 1.110223e-16 0.018449375 
		-0.020450994 1.110223e-16 -0.014412768 -0.020450994 1.110223e-16 0.01844937 -0.020450994 
		1.110223e-16 -0.014412774 -0.020450994 1.110223e-16 0.01844937 -0.020450994 1.110223e-16 
		-0.014412768 -0.020450994 1.110223e-16 0.01844937 -0.020450994 2.220446e-16 -0.014412768 
		-0.020450994 2.220446e-16 0.01844937 -0.020450994 2.220446e-16 -0.014412774 -0.020450994 
		2.220446e-16 0.01844937 -0.020450994 2.220446e-16 -0.014412774 -0.020450994 2.220446e-16 
		0.018433038 -0.020450994 0 -0.014429107 -0.020450994 0 -0.014412768 -0.020450994 
		2.220446e-16 0.01844937 -0.020450994 2.220446e-16 0.018384846 -0.020450994 0 -0.014479937 
		-0.020450994 0 0.0179445 -0.020450994 0 -0.014920192 -0.020450994 0 0.016753463 -0.020450994 
		0 -0.016132014 -0.020450994 0 0.015697595 -0.020450994 0 -0.017163597 -0.020450994 
		0 0.015708156 -0.020450994 0 -0.017175602 -0.020450994 0 0.018449375 -0.020450994 
		2.220446e-16 -0.014412768 -0.020450994 2.220446e-16 0.016108712 -0.020450994 0 -0.016774887 
		-0.020450994 0 0.016332796 -0.020450994 0 -0.016574791 -0.020450994 0 0.016038788 
		-0.020450994 0 -0.016891275 -0.020450994 0 0.015456538 -0.020450994 0 -0.017457383 
		-0.020450994 0 0.016151765 -0.020450994 0 -0.016719371 -0.020450994 0 -0.017531769 
		-0.020450994 0 0.015339373 -0.020450994 0 0.016719803 -0.020450994 0 -0.016157046 
		-0.020450994 0 0.016856836 -0.020450994 0 -0.01600324 -0.020450994 0 0.016325183 
		-0.020450994 0 -0.016534219 -0.020450994 0 0.015898988 -0.020450994 0 -0.016960487 
		-0.020450994 0 0.016293501 -0.020450994 0 -0.016565297 -0.020450994 0 0.016282205 
		-0.020450994 0 -0.016579196 -0.020450994 0 0.014663326 -0.020450994 0 -0.018226378 
		-0.020450994 0 0.014412897 -0.020450994 0 -0.018449375 -0.020450994 0 -0.018223643 
		-0.020450994 0 0.014643344 -0.020450994 0;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube2";
	rename -uid "D803A8F3-4A78-E99A-CC0B-FBA07EBC66EA";
	setAttr ".t" -type "double3" 0 1.4781301848788144 -0.86968151148956474 ;
	setAttr ".s" -type "double3" 4.3666437069669923 0.30456825922393932 8.4575883595983132 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "213EE155-424A-B668-EBEC-428239131147";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.87792003154754639 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt";
	setAttr ".pt[24]" -type "float3" 0.037842128 0 0.017237604 ;
	setAttr ".pt[25]" -type "float3" -0.037842128 0 0.017237604 ;
	setAttr ".pt[26]" -type "float3" -0.037842128 0 -0.017237604 ;
	setAttr ".pt[27]" -type "float3" 0.037842128 0 -0.017237604 ;
	setAttr ".pt[28]" -type "float3" -0.048783123 -4.4408921e-16 -0.022221377 ;
	setAttr ".pt[29]" -type "float3" 0.048783123 -4.4408921e-16 -0.022221377 ;
	setAttr ".pt[30]" -type "float3" 0.048783123 -4.4408921e-16 0.022221377 ;
	setAttr ".pt[31]" -type "float3" -0.048783123 -4.4408921e-16 0.022221377 ;
	setAttr ".pt[42]" -type "float3" 0.05909887 0 0.0010814441 ;
	setAttr ".pt[43]" -type "float3" -0.037842128 0 -0.00040267038 ;
	setAttr ".pt[44]" -type "float3" 0.037842128 0 -0.00040267038 ;
	setAttr ".pt[45]" -type "float3" -0.05909887 0 0.0010814441 ;
	setAttr ".pt[50]" -type "float3" 0.014858389 0 0.046294779 ;
	setAttr ".pt[51]" -type "float3" -0.0055324426 0 -0.017237604 ;
	setAttr ".pt[53]" -type "float3" -0.0055324426 0 0.017237604 ;
	setAttr ".pt[54]" -type "float3" 0.014858389 0 -0.046294734 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder1";
	rename -uid "C6328582-4538-CBE6-FA3F-24836EA6FCF3";
	setAttr ".t" -type "double3" 0 1.5725018576586509 -4.362535764640735 ;
	setAttr -av ".tx";
	setAttr -av ".ty";
	setAttr -av ".tz";
	setAttr ".s" -type "double3" 0.17796755233368367 0.28226979353850307 0.17796755233368367 ;
	setAttr -av ".sx";
	setAttr -av ".sy";
	setAttr -av ".sz";
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "38B7F97C-41AB-BDAF-52D6-6EB536BF3990";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999997019767761 0.76632368564605713 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 11 ".pt[21:31]" -type "float3"  -2.8312206e-07 3.7252912e-09 
		1.4901161e-07 -1.1175871e-07 3.7252912e-09 0 -5.3290705e-15 3.7252912e-09 0 5.9604645e-08 
		3.7252912e-09 0 0 3.7252912e-09 1.4901161e-07 -5.9604645e-08 3.7252912e-09 0 2.2351742e-07 
		3.7252912e-09 -2.9802322e-08 5.9604645e-08 3.7252912e-09 2.682209e-07 -1.1175871e-07 
		3.7252912e-09 2.682209e-07 -2.8312206e-07 3.7252912e-09 -2.9802322e-08 -5.9604645e-08 
		3.7252912e-09 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode polyExtrudeFace -n "polyExtrudeFace17";
	rename -uid "12D6E89B-4EA3-F275-B3AB-58BE47D7615B";
	setAttr ".ics" -type "componentList" 1 "f[756:796]";
	setAttr ".ix" -type "matrix" 1.1006372091180974 0 0 0 0 1 0 0 0 0 1.576247269613865 0
		 0 1.8385590966985761 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.0041539846 1.9704039 -0.8780027 ;
	setAttr ".rs" 48254;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.033817759351273097 1.6194093879507123 -4.7066443977667003 ;
	setAttr ".cbx" -type "double3" 0.042125728502887851 2.3213984426305831 2.9506390059304684 ;
	setAttr ".raf" no;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "DFC78CDF-47C8-8961-BF68-62BC54310B8B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "e[38:39]" "e[42:43]" "e[52]" "e[132:133]" "e[136:137]" "e[162:163]" "e[202:203]" "e[228:229]" "e[248:249]" "e[264:265]" "e[278:279]" "e[394:395]" "e[434:435]" "e[474:475]" "e[514:515]" "e[574:575]" "e[646:647]" "e[710:711]" "e[764:765]" "e[770:773]";
	setAttr ".ix" -type "matrix" 1.1006372091180974 0 0 0 0 1 0 0 0 0 1.576247269613865 0
		 0 1.8385590966985761 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak27";
	rename -uid "9E550DAF-49A8-FD13-D74A-9A80E9E592BE";
	setAttr ".uopa" yes;
	setAttr -s 216 ".tk";
	setAttr ".tk[62]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[63]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[64]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[68]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[78]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[87]" -type "float3" 0 -0.0057488321 0 ;
	setAttr ".tk[89]" -type "float3" 0 -0.0008828686 0 ;
	setAttr ".tk[119]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[120]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[121]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[125]" -type "float3" 0 -0.010399198 0 ;
	setAttr ".tk[129]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[130]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[131]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[135]" -type "float3" 0 -0.010114916 0 ;
	setAttr ".tk[139]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[140]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[141]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[145]" -type "float3" 0 -0.01065313 0 ;
	setAttr ".tk[149]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[150]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[151]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[155]" -type "float3" 0 -0.014451874 0 ;
	setAttr ".tk[159]" -type "float3" -0.02703907 -0.028448468 0.20136257 ;
	setAttr ".tk[160]" -type "float3" -0.13343063 -0.030391598 0.17840929 ;
	setAttr ".tk[161]" -type "float3" -0.16958795 -0.00076346012 0.068428315 ;
	setAttr ".tk[162]" -type "float3" -0.14033277 0.047139235 0.01447877 ;
	setAttr ".tk[163]" -type "float3" -0.026921365 0.060755052 0.0051549915 ;
	setAttr ".tk[164]" -type "float3" 0.10463995 0.022787249 -0.0692982 ;
	setAttr ".tk[165]" -type "float3" -0.013653757 0.037256431 -0.10986033 ;
	setAttr ".tk[166]" -type "float3" 0.14940198 0.017004179 0.0018244578 ;
	setAttr ".tk[167]" -type "float3" 0.088669449 0.048940741 0.014830744 ;
	setAttr ".tk[168]" -type "float3" 0.08582218 -0.030667169 0.18478669 ;
	setAttr ".tk[169]" -type "float3" 0.055750832 -0.066491418 0.13059762 ;
	setAttr ".tk[170]" -type "float3" -0.0015464306 -0.010735099 0.015476651 ;
	setAttr ".tk[171]" -type "float3" -0.021807604 -0.023538442 0.033723179 ;
	setAttr ".tk[172]" -type "float3" -0.14361024 0.020642294 -0.063276149 ;
	setAttr ".tk[173]" -type "float3" -0.022412563 -0.0072750803 -0.0066884402 ;
	setAttr ".tk[174]" -type "float3" -0.00027078064 -0.00083733274 -0.0036618803 ;
	setAttr ".tk[175]" -type "float3" 0.014323374 -0.0059830588 -0.0062016794 ;
	setAttr ".tk[176]" -type "float3" -0.1729102 0.0096862586 0.018941093 ;
	setAttr ".tk[177]" -type "float3" -0.019270144 -0.012026615 0.010828333 ;
	setAttr ".tk[178]" -type "float3" -0.032802835 -0.01574365 0.008609633 ;
	setAttr ".tk[179]" -type "float3" 0.14303653 -0.00041328638 0.08700525 ;
	setAttr ".tk[180]" -type "float3" 0.073869072 -0.030638235 0.01333109 ;
	setAttr ".tk[181]" -type "float3" 0.059651423 -0.036511578 0.044593222 ;
	setAttr ".tk[188]" -type "float3" 0 -0.090192713 0 ;
	setAttr ".tk[192]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[193]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[194]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[195]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[196]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[197]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[204]" -type "float3" 0 -0.0048170188 0 ;
	setAttr ".tk[207]" -type "float3" 0 -0.0048170188 0 ;
	setAttr ".tk[213]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[214]" -type "float3" 0.10766458 0.031346008 0.053852733 ;
	setAttr ".tk[215]" -type "float3" 0.11272041 -0.014122128 0.14016753 ;
	setAttr ".tk[223]" -type "float3" 0.15728977 0.0073175174 0.043802146 ;
	setAttr ".tk[224]" -type "float3" 0.12558432 0.00099072233 0.0050013126 ;
	setAttr ".tk[225]" -type "float3" 0.063449107 -0.030977465 0.026814504 ;
	setAttr ".tk[226]" -type "float3" 0.16236736 -0.017191563 0.097354278 ;
	setAttr ".tk[230]" -type "float3" 0.066046894 -0.052637432 0.085484199 ;
	setAttr ".tk[231]" -type "float3" 0.026116021 -0.010781984 -0.0020512934 ;
	setAttr ".tk[233]" -type "float3" 0 -0.00041880048 0 ;
	setAttr ".tk[239]" -type "float3" 0 -0.0027426851 0 ;
	setAttr ".tk[271]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[272]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[273]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[274]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[275]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[283]" -type "float3" 0 -0.0094478801 0 ;
	setAttr ".tk[284]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[289]" -type "float3" 0 -0.00073544501 0 ;
	setAttr ".tk[291]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[292]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[293]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[294]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[295]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[303]" -type "float3" 0 -0.010399198 0 ;
	setAttr ".tk[304]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[309]" -type "float3" 0 -0.00050883676 0 ;
	setAttr ".tk[311]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[312]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[313]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[314]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[315]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[316]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[317]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[328]" -type "float3" 0 -0.0041345311 0 ;
	setAttr ".tk[329]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[330]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[341]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[342]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[343]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[344]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[345]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[346]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[347]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[358]" -type "float3" 0 -0.013445451 0 ;
	setAttr ".tk[359]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[360]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[367]" -type "float3" 0 -0.00062391313 0 ;
	setAttr ".tk[368]" -type "float3" 0 -0.0023543313 0 ;
	setAttr ".tk[377]" -type "float3" 0 -0.090192713 0 ;
	setAttr ".tk[391]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[392]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[393]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[394]" -type "float3" 0 -0.012661127 0 ;
	setAttr ".tk[397]" -type "float3" 0 -0.090192713 0 ;
	setAttr ".tk[401]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[402]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[403]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[404]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[405]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[406]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[414]" -type "float3" 0 -0.0072614569 0 ;
	setAttr ".tk[415]" -type "float3" 0 -0.0072614569 0 ;
	setAttr ".tk[440]" -type "float3" -0.095815055 -0.027689669 0.19300768 ;
	setAttr ".tk[441]" -type "float3" -0.15196043 -0.0135021 0.1258183 ;
	setAttr ".tk[442]" -type "float3" -0.1550118 0.03080626 0.043523956 ;
	setAttr ".tk[443]" -type "float3" -0.10283892 0.058215648 0.0077165142 ;
	setAttr ".tk[444]" -type "float3" -0.02670153 0.019080048 0.10635476 ;
	setAttr ".tk[445]" -type "float3" 0.060944512 0.032843526 -0.092480145 ;
	setAttr ".tk[446]" -type "float3" 0.12647332 0.022902777 -0.037282549 ;
	setAttr ".tk[447]" -type "float3" 0.11180785 0.043873083 -0.0069083683 ;
	setAttr ".tk[448]" -type "float3" 0.044544488 0.058744431 0.0078197019 ;
	setAttr ".tk[449]" -type "float3" -0.019589525 0.054193806 -0.060175229 ;
	setAttr ".tk[504]" -type "float3" 0.041961942 -0.027746649 0.1947006 ;
	setAttr ".tk[505]" -type "float3" 0.096739553 -0.042910084 0.2025843 ;
	setAttr ".tk[506]" -type "float3" 0.0027030208 -0.01200253 0.01718731 ;
	setAttr ".tk[507]" -type "float3" -0.016970782 -0.054156426 0.15538959 ;
	setAttr ".tk[510]" -type "float3" -0.0052430434 -0.010143381 0.014416989 ;
	setAttr ".tk[511]" -type "float3" -0.116341 -0.046502262 0.16305001 ;
	setAttr ".tk[518]" -type "float3" -0.097121403 0.03225787 -0.090890631 ;
	setAttr ".tk[519]" -type "float3" -0.1304982 0.015609629 -0.056757018 ;
	setAttr ".tk[520]" -type "float3" -0.0053569865 -0.0019723903 -0.0052946312 ;
	setAttr ".tk[521]" -type "float3" -0.0086660935 0.029555069 -0.11289769 ;
	setAttr ".tk[524]" -type "float3" 0.0030855816 -0.001631952 -0.0044073621 ;
	setAttr ".tk[525]" -type "float3" 0.10362274 0.016755551 -0.065348767 ;
	setAttr ".tk[567]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[568]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[569]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[570]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[571]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[572]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[573]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[596]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[597]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[598]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[599]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[600]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[601]" -type "float3" -0.15578046 0.039477881 0.0024567365 ;
	setAttr ".tk[602]" -type "float3" -0.16175394 0.018076869 -0.023733113 ;
	setAttr ".tk[613]" -type "float3" 0 -0.00018605158 0 ;
	setAttr ".tk[614]" -type "float3" -0.15046948 0.00040637128 0.039294254 ;
	setAttr ".tk[615]" -type "float3" -0.15718272 -0.017989906 0.068992235 ;
	setAttr ".tk[616]" -type "float3" -0.031474315 -0.016753897 0.012398688 ;
	setAttr ".tk[617]" -type "float3" -0.1619518 -0.0059027695 0.021709722 ;
	setAttr ".tk[619]" -type "float3" 0 -0.0033948068 0 ;
	setAttr ".tk[621]" -type "float3" -0.013985701 -0.011346913 0.013272142 ;
	setAttr ".tk[622]" -type "float3" -0.025627287 -0.010050991 0.00041626539 ;
	setAttr ".tk[625]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[626]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[636]" -type "float3" -0.1093306 0.017135054 0.096517652 ;
	setAttr ".tk[637]" -type "float3" 0.066702828 0.048324019 -0.046450365 ;
	setAttr ".tk[663]" -type "float3" 0.025283191 -0.052813955 0.15528032 ;
	setAttr ".tk[665]" -type "float3" -0.058503926 -0.052386206 0.15057462 ;
	setAttr ".tk[669]" -type "float3" -0.080709331 0.025135068 -0.097261995 ;
	setAttr ".tk[671]" -type "float3" 0.054584306 0.025014605 -0.099247873 ;
	setAttr ".tk[695]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[696]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[705]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[706]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[707]" -type "float3" -0.11040644 0.045981444 -0.040092353 ;
	setAttr ".tk[711]" -type "float3" -0.14336853 -0.011638287 0.039544147 ;
	setAttr ".tk[713]" -type "float3" -0.12564848 -0.032475818 0.10546973 ;
	setAttr ".tk[714]" -type "float3" -0.13017139 0.0038817131 -0.012669344 ;
	setAttr ".tk[717]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[722]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[724]" -type "float3" -0.17179294 0.026629366 0.028055491 ;
	setAttr ".tk[725]" -type "float3" 0.056827668 0.016907392 0.1032887 ;
	setAttr ".tk[728]" -type "float3" 0.1729102 -0.0064744102 0.047453504 ;
	setAttr ".tk[730]" -type "float3" 0.13741146 -0.030782595 0.15377174 ;
	setAttr ".tk[731]" -type "float3" 0.10106175 0.0086537274 -0.022142692 ;
	setAttr ".tk[737]" -type "float3" 0 -0.0018933511 0 ;
	setAttr ".tk[739]" -type "float3" 0.13451199 0.030154457 0.028743843 ;
	setAttr ".tk[760]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[761]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[765]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[768]" -type "float3" 0 -0.001302245 0 ;
	setAttr ".tk[770]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[771]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[775]" -type "float3" 0 -0.058642216 0 ;
	setAttr ".tk[778]" -type "float3" 0 -0.0008828686 0 ;
	setAttr ".tk[780]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[781]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[785]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[790]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[791]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[795]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[798]" -type "float3" 0 -0.0018933511 0 ;
	setAttr ".tk[810]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[811]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[812]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[819]" -type "float3" 0 -0.001184123 0 ;
	setAttr ".tk[820]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[821]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[822]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[823]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[830]" -type "float3" 0 -0.05893711 0 ;
	setAttr ".tk[831]" -type "float3" 0 -0.05893711 0 ;
createNode polySmoothFace -n "polySmoothFace1";
	rename -uid "6A8F2BBA-4EF8-873E-D0E2-0AABA9AF519C";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".sdt" 2;
	setAttr ".suv" yes;
	setAttr ".ps" 0.10000000149011612;
	setAttr ".ro" 1;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode polyTweak -n "polyTweak21";
	rename -uid "CA2D26A9-4CE8-1E92-A25C-829F54AA5333";
	setAttr ".uopa" yes;
	setAttr -s 69 ".tk";
	setAttr ".tk[62]" -type "float3" -0.034158044 -0.056543294 -0.029563045 ;
	setAttr ".tk[63]" -type "float3" 0.0078246929 -0.067550309 -0.029563045 ;
	setAttr ".tk[64]" -type "float3" 0.049807403 -0.056543294 -0.029563045 ;
	setAttr ".tk[65]" -type "float3" 0.057463985 0.041505832 -0.029563066 ;
	setAttr ".tk[66]" -type "float3" 7.5977914e-05 0.067550302 -0.029563045 ;
	setAttr ".tk[67]" -type "float3" -0.041814581 0.041505832 -0.029563066 ;
	setAttr ".tk[74]" -type "float3" 0.047580436 -0.029813616 -0.00011022038 ;
	setAttr ".tk[75]" -type "float3" 0.00024421263 -0.039278634 -0.00011025642 ;
	setAttr ".tk[76]" -type "float3" -0.047092017 -0.029813616 -0.00011022038 ;
	setAttr ".tk[77]" -type "float3" -0.039790694 0.032305177 -0.00011025642 ;
	setAttr ".tk[78]" -type "float3" 0.00024421263 0.039278634 -0.00011025642 ;
	setAttr ".tk[79]" -type "float3" 0.040279128 0.032305177 -0.00011025642 ;
	setAttr ".tk[86]" -type "float3" 0.057463985 0.0045085023 -0.029563045 ;
	setAttr ".tk[87]" -type "float3" 0.05686117 -0.0044515729 -0.029563045 ;
	setAttr ".tk[88]" -type "float3" -0.05559415 -0.006377175 -0.00024552396 ;
	setAttr ".tk[89]" -type "float3" -0.055024441 -0.00071576802 -0.00028500266 ;
	setAttr ".tk[95]" -type "float3" -0.055449348 -0.014984293 -0.02885706 ;
	setAttr ".tk[96]" -type "float3" -0.057463974 0.015175932 -0.029098636 ;
	setAttr ".tk[97]" -type "float3" 0.05559415 -0.013144207 0.00028500266 ;
	setAttr ".tk[98]" -type "float3" 0.053685267 0.0059150825 5.6074066e-05 ;
	setAttr ".tk[120]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".tk[122]" -type "float3" 0 6.9849193e-10 0 ;
	setAttr ".tk[123]" -type "float3" 0 -7.4505806e-09 0 ;
	setAttr ".tk[124]" -type "float3" 0 6.9849193e-10 0 ;
	setAttr ".tk[126]" -type "float3" 0 2.3283064e-10 0 ;
	setAttr ".tk[128]" -type "float3" 0 2.2118911e-09 0 ;
	setAttr ".tk[139]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[140]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[141]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[142]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[143]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[144]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[145]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[146]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[147]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[148]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[149]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[150]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[151]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[152]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[153]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[154]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[155]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[156]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[157]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[158]" -type "float3" 0 0 0.10550762 ;
	setAttr ".tk[162]" -type "float3" 0 -0.061991751 0.18782119 ;
	setAttr ".tk[163]" -type "float3" 0 -0.061991751 0.18782119 ;
	setAttr ".tk[167]" -type "float3" 0 -0.061991751 0.18782119 ;
	setAttr ".tk[192]" -type "float3" -0.057895605 -0.058955628 0.016112216 ;
	setAttr ".tk[193]" -type "float3" 0.013262338 -0.070432238 0.016112216 ;
	setAttr ".tk[194]" -type "float3" 0.013262338 -0.070432238 -0.013045242 ;
	setAttr ".tk[195]" -type "float3" -0.057895605 -0.058955628 -0.013045242 ;
	setAttr ".tk[196]" -type "float3" 0.084420241 -0.058955628 0.016112216 ;
	setAttr ".tk[197]" -type "float3" 0.084420241 -0.058955628 -0.013045242 ;
	setAttr ".tk[198]" -type "float3" 0.00012877758 0.070432238 0.016112216 ;
	setAttr ".tk[199]" -type "float3" 0.097397618 0.043276604 0.016112359 ;
	setAttr ".tk[200]" -type "float3" 0.00012877758 0.070432238 -0.013045242 ;
	setAttr ".tk[201]" -type "float3" 0.097397618 0.043276604 -0.013045143 ;
	setAttr ".tk[202]" -type "float3" -0.07087297 0.043276604 0.016112359 ;
	setAttr ".tk[203]" -type "float3" -0.07087297 0.043276604 -0.013045143 ;
	setAttr ".tk[204]" -type "float3" 0.096375898 -0.0046414915 0.016112216 ;
	setAttr ".tk[205]" -type "float3" 0.097397618 0.0047008498 0.016112216 ;
	setAttr ".tk[206]" -type "float3" 0.097397618 0.0047008498 -0.013045242 ;
	setAttr ".tk[207]" -type "float3" 0.096375898 -0.0046414915 -0.013045242 ;
	setAttr ".tk[208]" -type "float3" -0.097397618 0.015823388 0.014094651 ;
	setAttr ".tk[209]" -type "float3" -0.093982972 -0.015623573 0.013045143 ;
	setAttr ".tk[210]" -type "float3" -0.093982972 -0.015623573 -0.016112359 ;
	setAttr ".tk[211]" -type "float3" -0.097397618 0.015823388 -0.015062861 ;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "AE36A0A0-4D8D-E6FD-AA72-7CBF778912EE";
	setAttr ".ics" -type "componentList" 1 "f[149:158]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.016810149 0.97763014 -1.2637167 ;
	setAttr ".rs" 61464;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.61348915100097656 0.54614692579660296 -1.3022263725856966 ;
	setAttr ".cbx" -type "double3" 0.5798688530921936 1.4091134036675441 -1.2252069195369906 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak20";
	rename -uid "99105013-4984-AC5B-E167-9CB24FBF1961";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[182:191]" -type "float3"  -0.053965688 -0.040450491
		 0.11452292 0.0123621 -0.065655053 0.13681653 0.078689873 -0.040450491 0.11452292
		 0.08983399 0.078833774 0.009016254 0.090786323 0.099351637 -0.0091315685 0.090786323
		 0.18407133 -0.084066048 0.00012003625 0.24371032 -0.13681653 -0.06606213 0.18407133
		 -0.084066048 -0.090786323 0.12519579 -0.030251626 -0.087603517 0.056868929 0.0310877;
createNode polySplit -n "polySplit6";
	rename -uid "DD2E8BC2-43EA-6C85-8FEF-13847630C7A1";
	setAttr -s 11 ".e[0:10]"  0.46543801 0.46543801 0.46543801 0.46543801
		 0.46543801 0.46543801 0.46543801 0.46543801 0.46543801 0.46543801 0.46543801;
	setAttr -s 11 ".d[0:10]"  -2147483326 -2147483324 -2147483321 -2147483306 -2147483304 -2147483317 
		-2147483315 -2147483312 -2147483296 -2147483294 -2147483326;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak19";
	rename -uid "F91C9E27-4C63-6D63-91F7-CEAE2754A392";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk[159:181]" -type "float3"  7.4505806e-09 -0.20296527
		 1.21879447 -3.7252903e-09 -0.23702288 1.27201164 -4.6566129e-10 -0.39679441 1.52166224
		 0 -0.41197929 1.54539108 0 -0.41197935 1.54539096 1.8626451e-09 -0.54040521 1.74606574
		 0 -0.6209926 1.87198615 -1.8626451e-09 -0.47157818 1.62038291 1.1641532e-10 -0.41197929
		 1.54539108 3.7252903e-09 -0.23702268 1.27201223 0 -0.19211456 1.26336241 0 -0.15475257
		 1.20498264 0 -0.19211459 1.26336229 -3.7252903e-09 -0.54040521 1.74606574 0 -0.52493417
		 1.78341281 0 -0.61334026 1.9215517 1.8626451e-09 -0.52493417 1.78341281 5.8207661e-11
		 -0.42723861 1.56923461 0 -0.36893591 1.53965545 -5.8207661e-11 -0.39934999 1.58718002
		 0 -0.37076157 1.45918858 0 -0.43956411 1.64119768 0 -0.33927071 1.479895;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "F697C297-4489-EE0D-DB9B-E3B8A0B52A20";
	setAttr ".ics" -type "componentList" 12 "f[5:6]" "f[32]" "f[34]" "f[38]" "f[40]" "f[76]" "f[80]" "f[82:83]" "f[93:94]" "f[97]" "f[99:100]" "f[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.016810149 0.97763014 -1.2097589 ;
	setAttr ".rs" 53235;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.56243985891342163 0.58306279431733965 -1.2305889805416292 ;
	setAttr ".cbx" -type "double3" 0.52881956100463867 1.3721975351468074 -1.1889286717037386 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak18";
	rename -uid "3C9000C2-4F7B-AC47-30EA-2CB0BFF538CD";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk[149:171]" -type "float3"  -0.0069512306 0.033650812
		 -0.00092919351 -0.04094876 0.02816757 -0.00092913205 -0.046610869 0.0024447683 -0.00092919351
		 -0.04392137 2.5848461e-09 -0.00092919351 -0.0069512306 2.5848461e-09 -0.00092919351
		 0.033246543 -0.020676514 -0.00092909177 -0.0016611071 -0.033650812 -0.00092919351
		 0.044571992 -0.0082697766 -0.0017775077 0.022382105 2.5848461e-09 -0.00092919351
		 0.027046297 0.02816757 -0.00092913205 0.030345002 0.030900609 0.0019488537 -0.0069512306
		 0.036915872 0.0019488537 -0.044247467 0.030900609 0.0019488537 -0.047149014 -0.020676514
		 -0.00092909177 -0.051049318 -0.022682702 0.0019488728 -6.7496614e-05 -0.036915872
		 0.0019488537 0.037146859 -0.022682702 0.0019488728 -0.047149014 -0.0024568033 -0.00092917238
		 -0.050513819 0.0024327589 0.0019488537 -0.051049318 -0.0024638716 0.0019488537 0.042768687
		 0.0082292501 -0.0019488728 0.051049318 -0.0082935616 0.0015362821 0.049259614 0.0081888316
		 0.0013216748;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "9F04A90E-41ED-7FC1-D6DB-3C8ADEF95C9F";
	setAttr ".ics" -type "componentList" 12 "f[5:6]" "f[32]" "f[34]" "f[38]" "f[40]" "f[76]" "f[80]" "f[82:83]" "f[93:94]" "f[97]" "f[99:100]" "f[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.016810149 0.97763014 -1.2097589 ;
	setAttr ".rs" 51453;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.61348915100097656 0.54614692579660296 -1.2325378140072054 ;
	setAttr ".cbx" -type "double3" 0.5798688530921936 1.4091134036675441 -1.1869798382381624 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak17";
	rename -uid "9445FD40-4FD4-8D8B-13EE-D0BD5F968099";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk[139:161]" -type "float3"  0 0 0.069688492 0 0 0.069688492
		 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0
		 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0
		 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492
		 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492 0 0 0.069688492;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "E778F051-4E40-A203-80E6-1C8C5CF09E45";
	setAttr ".ics" -type "componentList" 12 "f[5:6]" "f[32]" "f[34]" "f[38]" "f[40]" "f[76]" "f[80]" "f[82:83]" "f[93:94]" "f[97]" "f[99:100]" "f[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.016810149 0.97763014 -1.2794474 ;
	setAttr ".rs" 44305;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.61348915100097656 0.54614692579660296 -1.3022263725856966 ;
	setAttr ".cbx" -type "double3" 0.5798688530921936 1.4091134036675441 -1.2566683968166537 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak16";
	rename -uid "9A1A173D-4095-21F5-F111-E5847C23E152";
	setAttr ".uopa" yes;
	setAttr -s 43 ".tk";
	setAttr ".tk[62]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[63]" -type "float3" 0.064437822 0.092323832 -0.4444139 ;
	setAttr ".tk[64]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[65]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[66]" -type "float3" 0.064437822 -0.17982554 -0.4444139 ;
	setAttr ".tk[67]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[86]" -type "float3" 0.064437822 -0.05283286 -0.4444139 ;
	setAttr ".tk[87]" -type "float3" 0.064437822 -0.034783509 -0.4444139 ;
	setAttr ".tk[95]" -type "float3" 0.064437822 -0.013566196 -0.4444139 ;
	setAttr ".tk[96]" -type "float3" 0.064437822 -0.074321546 -0.4444139 ;
	setAttr ".tk[119]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[120]" -type "float3" 0.064437822 0.092323832 -0.4444139 ;
	setAttr ".tk[121]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[122]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[123]" -type "float3" 0.064437822 -0.17982554 -0.4444139 ;
	setAttr ".tk[124]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[125]" -type "float3" 0.064437822 -0.034783509 -0.4444139 ;
	setAttr ".tk[126]" -type "float3" 0.064437822 -0.05283286 -0.4444139 ;
	setAttr ".tk[127]" -type "float3" 0.064437822 -0.074321546 -0.4444139 ;
	setAttr ".tk[128]" -type "float3" 0.064437822 -0.013566196 -0.4444139 ;
	setAttr ".tk[129]" -type "float3" 0.064437822 0.080288582 -0.4444139 ;
	setAttr ".tk[130]" -type "float3" 0.064437822 0.060076933 -0.4444139 ;
	setAttr ".tk[131]" -type "float3" 0.064437822 -0.034739252 -0.4444139 ;
	setAttr ".tk[132]" -type "float3" 0.064437822 -0.043750852 -0.4444139 ;
	setAttr ".tk[133]" -type "float3" 0.064437822 -0.043750852 -0.4444139 ;
	setAttr ".tk[134]" -type "float3" 0.064437822 -0.11996604 -0.4444139 ;
	setAttr ".tk[135]" -type "float3" 0.064437822 -0.16779029 -0.4444139 ;
	setAttr ".tk[136]" -type "float3" 0.064437822 -0.074233882 -0.4444139 ;
	setAttr ".tk[137]" -type "float3" 0.064437822 -0.043750852 -0.4444139 ;
	setAttr ".tk[138]" -type "float3" 0.064437822 0.060076933 -0.4444139 ;
	setAttr ".tk[139]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[140]" -type "float3" 0.064437822 0.092323832 -0.4444139 ;
	setAttr ".tk[141]" -type "float3" 0.064437822 0.070151113 -0.4444139 ;
	setAttr ".tk[142]" -type "float3" 0.064437822 -0.11996604 -0.4444139 ;
	setAttr ".tk[143]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[144]" -type "float3" 0.064437822 -0.17982554 -0.4444139 ;
	setAttr ".tk[145]" -type "float3" 0.064437822 -0.12736103 -0.4444139 ;
	setAttr ".tk[146]" -type "float3" 0.064437822 -0.052806821 -0.4444139 ;
	setAttr ".tk[147]" -type "float3" 0.064437822 -0.034783509 -0.4444139 ;
	setAttr ".tk[148]" -type "float3" 0.064437822 -0.05283286 -0.4444139 ;
	setAttr ".tk[149]" -type "float3" 0.064437822 -0.013417207 -0.4444139 ;
	setAttr ".tk[150]" -type "float3" 0.064437822 -0.074321546 -0.4444139 ;
	setAttr ".tk[151]" -type "float3" 0.064437822 -0.013566196 -0.4444139 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "0DCD5CDA-42CD-AD9B-ED8D-858CCE231480";
	setAttr ".ics" -type "componentList" 12 "f[5:6]" "f[32]" "f[34]" "f[38]" "f[40]" "f[76]" "f[80]" "f[82:83]" "f[93:94]" "f[97]" "f[99:100]" "f[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.081247956 1.021381 -0.83503348 ;
	setAttr ".rs" 42802;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.67792695760726929 0.45382308613214373 -0.85781247221033041 ;
	setAttr ".cbx" -type "double3" 0.51543104648590088 1.5889389480248439 -0.81225449644128744 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak15";
	rename -uid "B5DD42C7-48D2-EAC6-CE8A-719D735E14F1";
	setAttr ".uopa" yes;
	setAttr -s 33 ".tk";
	setAttr ".tk[62]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[63]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[64]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[65]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[66]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[67]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[86]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[87]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[95]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[96]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[119]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[120]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[121]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[122]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[123]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[124]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[125]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[126]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[127]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[128]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[129]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[130]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[131]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[132]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[133]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[134]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[135]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[136]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[137]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[138]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[139]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[140]" -type "float3" 0 0 0.085521169 ;
	setAttr ".tk[141]" -type "float3" 0 0 0.085521169 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "0DAE8BD7-4E7E-0223-F3A6-62B75AC504AB";
	setAttr ".ics" -type "componentList" 12 "f[5:6]" "f[32]" "f[34]" "f[38]" "f[40]" "f[76]" "f[80]" "f[82:83]" "f[93:94]" "f[97]" "f[99:100]" "f[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.081247956 1.021381 -0.9205547 ;
	setAttr ".rs" 62146;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.67792695760726929 0.45382308613214373 -0.94333369337121908 ;
	setAttr ".cbx" -type "double3" 0.51543104648590088 1.5889389480248439 -0.89777571760217612 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak14";
	rename -uid "78521866-4692-8A46-7310-F990140D0B59";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[67]" -type "float3" -0.061832476 0 0 ;
	setAttr ".tk[73]" -type "float3" -0.080459051 0 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.093302175 0 ;
	setAttr ".tk[82]" -type "float3" 0 -0.093302175 0 ;
createNode polySplit -n "polySplit5";
	rename -uid "F8809D30-4DCF-93DD-D28D-B2812D5435C2";
	setAttr -s 11 ".e[0:10]"  0.78419602 0.78419602 0.78419602 0.215804
		 0.78419602 0.78419602 0.78419602 0.78419602 0.215804 0.78419602 0.78419602;
	setAttr -s 11 ".d[0:10]"  -2147483522 -2147483520 -2147483517 -2147483426 -2147483475 -2147483514 
		-2147483512 -2147483509 -2147483421 -2147483447 -2147483522;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak13";
	rename -uid "CD46EC11-45E9-4DA2-464F-E7B4C4DBF002";
	setAttr ".uopa" yes;
	setAttr ".tk[118]" -type "float3"  0 -0.12542957 0;
createNode polySplit -n "polySplit4";
	rename -uid "F8EF6DB1-4923-52A2-F249-E6A3F7BBA802";
	setAttr -s 11 ".e[0:10]"  0.64775902 0.64775902 0.64775902 0.35224101
		 0.64775902 0.64775902 0.64775902 0.64775902 0.35224101 0.64775902 0.64775902;
	setAttr -s 11 ".d[0:10]"  -2147483522 -2147483520 -2147483517 -2147483473 -2147483475 -2147483514 
		-2147483512 -2147483509 -2147483445 -2147483447 -2147483522;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "B4B46B6C-4AEE-922D-2FBC-85B964FE78F2";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk";
	setAttr ".tk[100]" -type "float3" 0.093137905 0 -0.0073306262 ;
	setAttr ".tk[101]" -type "float3" 0.0061490098 0 0.00026591547 ;
	setAttr ".tk[102]" -type "float3" 0.092924029 0 -0.0037507778 ;
	setAttr ".tk[103]" -type "float3" -0.095231146 0 -0.0033441056 ;
	setAttr ".tk[104]" -type "float3" -0.095045418 0 -0.0064528733 ;
	setAttr ".tk[105]" -type "float3" -0.13247 0 0.011918564 ;
	setAttr ".tk[107]" -type "float3" -0.13237506 0 0.0099157579 ;
	setAttr ".tk[108]" -type "float3" -0.1626458 0 0.0073306281 ;
	setAttr ".tk[109]" -type "float3" -0.16249591 0 0.0048221536 ;
	setAttr ".tk[110]" -type "float3" -0.10627654 0 -0.005241639 ;
	setAttr ".tk[111]" -type "float3" -0.10645796 0 -0.002205936 ;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "FFD65099-4012-AA2F-F0A1-789E042FCD4C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[129:130]" "e[149]" "e[162]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.4;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak11";
	rename -uid "511EDD92-403C-DCC2-A0EA-8E8C7386FBFC";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[94]" -type "float3" -0.0028090067 0 -0.0016978468 ;
	setAttr ".tk[95]" -type "float3" -0.0028304495 0 -0.0023174174 ;
	setAttr ".tk[96]" -type "float3" 0.10627858 0 0.0014645844 ;
	setAttr ".tk[97]" -type "float3" 0.10626003 0 0.00092825585 ;
	setAttr ".tk[103]" -type "float3" 0.11275396 0 0.0017938332 ;
	setAttr ".tk[104]" -type "float3" 0.11277203 0 0.0023174174 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "50681008-42D5-7A00-1212-F7A919D32D89";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[115]" "e[147]" "e[149]" "e[164]" "e[180]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 1;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak10";
	rename -uid "8B0268BD-4059-78CE-52B8-57A78E6EA096";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[82]" -type "float3" -0.067959398 0 0 ;
	setAttr ".tk[83]" -type "float3" -0.057477083 0 0 ;
	setAttr ".tk[85]" -type "float3" 0.057477083 0 0 ;
	setAttr ".tk[86]" -type "float3" 0.067959398 0 0 ;
	setAttr ".tk[87]" -type "float3" 0.067959398 0 0 ;
	setAttr ".tk[89]" -type "float3" -0.067959398 0 0 ;
	setAttr ".tk[90]" -type "float3" -0.062254552 0 0 ;
	setAttr ".tk[91]" -type "float3" -0.062254552 0 0 ;
	setAttr ".tk[93]" -type "float3" 0.062254552 0 0 ;
	setAttr ".tk[94]" -type "float3" 0.062254552 0 0 ;
	setAttr ".tk[95]" -type "float3" 0.052652173 0 0 ;
	setAttr ".tk[97]" -type "float3" -0.052652173 0 0 ;
createNode polySplit -n "polySplit3";
	rename -uid "E00FFB33-4269-3020-5418-8D98CEF33116";
	setAttr -s 9 ".e[0:8]"  0.167106 0.167106 0.167106 0.167106 0.167106
		 0.167106 0.167106 0.167106 0.167106;
	setAttr -s 9 ".d[0:8]"  -2147483488 -2147483481 -2147483482 -2147483483 -2147483484 -2147483485 
		-2147483486 -2147483487 -2147483488;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "5B6C49B0-4889-3FC6-89DC-7E8982971530";
	setAttr -s 9 ".e[0:8]"  0.73803502 0.73803502 0.73803502 0.73803502
		 0.73803502 0.73803502 0.73803502 0.73803502 0.73803502;
	setAttr -s 9 ".d[0:8]"  -2147483512 -2147483510 -2147483507 -2147483504 -2147483501 -2147483498 
		-2147483495 -2147483492 -2147483512;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak9";
	rename -uid "B6AE4AFE-408C-C1B0-DF01-3F8FF7EF4029";
	setAttr ".uopa" yes;
	setAttr -s 33 ".tk[49:81]" -type "float3"  0.080578856 0 -0.02403691
		 0.068150073 0.081345379 -0.02403691 0 0.097180501 -0.02403691 -0.068150073 0.081345379
		 -0.02403691 -0.080578856 0 -0.02403691 -0.080578856 -0.0597119 -0.024036858 0 -0.097180501
		 -0.02403691 0.080578856 -0.0597119 -0.024036858 -0.2295243 0 0 -0.19412158 0 0 0
		 0 0 0.19412158 0 0 0.2295243 0 0 0.2295243 0 0 0 0 0 -0.2295243 0 0 0 0 2.13333893
		 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893
		 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893
		 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893 0 0 2.13333893;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "DA89EF3A-4A8B-43C0-BFDC-048AAEC878ED";
	setAttr ".ics" -type "componentList" 10 "f[0]" "f[6:8]" "f[33]" "f[35]" "f[37]" "f[39]" "f[41]" "f[43]" "f[45]" "f[47]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.021381 -3.0598528 ;
	setAttr ".rs" 36670;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.51543104648590088 0.45382308613214373 -3.0766727408031649 ;
	setAttr ".cbx" -type "double3" 0.51543104648590088 1.5889389480248439 -3.0430330117801851 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak8";
	rename -uid "BAF78DE2-49B1-1447-4018-0AA1D86DC0B6";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk[57:73]" -type "float3"  -0.064930394 -0.036248054
		 0.077158645 -0.076772012 0 0.077158846 0 -0.043304306 0.077158846 0 0 0.077158846
		 0.064930394 -0.036248054 0.077158645 0.076772012 0 0.077158846 0.076772012 0.026608054
		 0.077158622 0 0.043304306 0.077158846 -0.076772012 0.026608054 0.077158622 -0.084221058
		 0 0.071662173 -0.071230471 -0.039765112 0.071662173 0 -0.04750599 0.071662173 0.071230471
		 -0.039765112 0.071662173 0.084221058 0 0.071662173 0.084221058 0.029189738 0.071662046
		 0 0.04750599 0.071662173 -0.084221058 0.029189738 0.071662046;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "D822B369-4B1E-19CC-1C78-F1A1E2ECDAEF";
	setAttr ".ics" -type "componentList" 10 "f[0]" "f[6:8]" "f[33]" "f[35]" "f[37]" "f[39]" "f[41]" "f[43]" "f[45]" "f[47]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.021381 -3.1342635 ;
	setAttr ".rs" 62187;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.4312099814414978 0.50132906090173601 -3.1483348687748141 ;
	setAttr ".cbx" -type "double3" 0.4312099814414978 1.5414329732552516 -3.1201919396976656 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak7";
	rename -uid "56D7CFE4-49DD-DFDE-7749-E9B84CD00EFF";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk[49:65]" -type "float3"  0 0 0.043305561 0 0 0.043305561
		 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0
		 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0
		 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561 0 0 0.043305561;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "24792879-4845-9949-0BA2-389AA0A32C5F";
	setAttr ".ics" -type "componentList" 10 "f[0]" "f[6:8]" "f[33]" "f[35]" "f[37]" "f[39]" "f[41]" "f[43]" "f[45]" "f[47]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.021381 -3.1775692 ;
	setAttr ".rs" 60885;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.4312099814414978 0.50132906090173601 -3.1916405638317293 ;
	setAttr ".cbx" -type "double3" 0.4312099814414978 1.5414329732552516 -3.1634975751499361 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak6";
	rename -uid "E2CEDF87-411F-677D-BDD7-768A7871A06E";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[25]" -type "float3" -0.018822396 -0.022466812 0.00079669134 ;
	setAttr ".tk[26]" -type "float3" -0.022255108 0 0.00079670589 ;
	setAttr ".tk[27]" -type "float3" 0 -0.026840318 0.00079670589 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.00079670589 ;
	setAttr ".tk[29]" -type "float3" 0.018822396 -0.022466812 0.00079669134 ;
	setAttr ".tk[30]" -type "float3" 0.022255108 0 0.00079670589 ;
	setAttr ".tk[31]" -type "float3" 0.022255108 0.016491851 0.00079666288 ;
	setAttr ".tk[32]" -type "float3" 0 0.026840318 0.00079670589 ;
	setAttr ".tk[33]" -type "float3" -0.022255108 0.016491851 0.00079666288 ;
	setAttr ".tk[36]" -type "float3" -0.020648694 -0.024646722 -0.00079669134 ;
	setAttr ".tk[37]" -type "float3" -0.024414472 0 -0.00079668406 ;
	setAttr ".tk[39]" -type "float3" 0 -0.029444573 -0.00079668406 ;
	setAttr ".tk[41]" -type "float3" 0.020648694 -0.024646722 -0.00079669134 ;
	setAttr ".tk[43]" -type "float3" 0.024414472 0 -0.00079668406 ;
	setAttr ".tk[45]" -type "float3" 0.024414472 0.018092018 -0.00079670589 ;
	setAttr ".tk[47]" -type "float3" 0 0.029444573 -0.00079668406 ;
	setAttr ".tk[49]" -type "float3" -0.024414472 0.018092018 -0.00079670589 ;
createNode polySplit -n "polySplit1";
	rename -uid "576BD6B0-472A-3F3F-9B75-FD8C9FE8EC51";
	setAttr -s 9 ".e[0:8]"  0.057036702 0.057036702 0.057036702 0.057036702
		 0.057036702 0.057036702 0.057036702 0.057036702 0.057036702;
	setAttr -s 9 ".d[0:8]"  -2147483592 -2147483584 -2147483579 -2147483574 -2147483569 -2147483564 
		-2147483559 -2147483591 -2147483592;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "10FB0938-4AE3-490E-3D32-A9BD905D2FAA";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[34:49]" -type "float3"  -0.030429859 -0.036321729
		 0.0265489 -0.035979472 0 0.026548782 -0.030429859 -0.036321729 -0.026549054 -0.035979472
		 0 -0.026549175 0 -0.043392297 0.026548782 0 -0.043392297 -0.026549175 0.030429859
		 -0.036321729 0.0265489 0.030429859 -0.036321729 -0.026549054 0.035979472 0 0.026548782
		 0.035979472 0 -0.026549175 0.035979472 0.0266621 0.026549142 0.035979472 0.0266621
		 -0.026548808 0 0.043392297 0.026548782 0 0.043392297 -0.026549175 -0.035979472 0.0266621
		 0.026549142 -0.035979472 0.0266621 -0.026548808;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "D05FD09E-4DBC-7B07-81CE-5FB9851E73B9";
	setAttr ".ics" -type "componentList" 1 "f[24:31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.021381 -3.2169752 ;
	setAttr ".rs" 43027;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.37081602215766907 0.57416593681726336 -3.2696562489132113 ;
	setAttr ".cbx" -type "double3" 0.37081602215766907 1.4685960973397243 -3.1642943700412935 ;
	setAttr ".raf" no;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "3BCBF040-40E2-15EE-7008-A99C2ACB440F";
	setAttr ".ics" -type "componentList" 2 "vtx[21]" "vtx[25]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "9EAF2728-4081-D9EE-3060-AFA663F2DADB";
	setAttr ".uopa" yes;
	setAttr ".tk[21]" -type "float3"  0 0.10481498 0;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "6EE83D14-4B6D-73AF-F317-9187E982973A";
	setAttr ".ics" -type "componentList" 2 "vtx[7]" "vtx[25]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "72193CAD-4E89-5DCF-D08B-C2A175C79226";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[7]" -type "float3" 0 0.090599939 0 ;
	setAttr ".tk[27]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[29]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[30]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[31]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.10536116 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.10536116 ;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "90F860B8-430F-4BB8-B667-F18C8E5ACCFB";
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[6:8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.021381 -3.2696559 ;
	setAttr ".rs" 54015;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.37081602215766907 0.57416593681726336 -3.2696561595062441 ;
	setAttr ".cbx" -type "double3" 0.37081602215766907 1.4685960973397243 -3.2696556230644411 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak2";
	rename -uid "C771DA99-4FEB-CB5E-3141-1A9625DDD1AD";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[0]" -type "float3" 0.10096358 0.12565652 -0.0315485 ;
	setAttr ".tk[1]" -type "float3" -0.10096358 0.12565652 -0.0315485 ;
	setAttr ".tk[2]" -type "float3" 0.043767523 -0.032433342 0.031548344 ;
	setAttr ".tk[3]" -type "float3" -0.043767523 -0.032433342 0.031548344 ;
	setAttr ".tk[6]" -type "float3" 0.068660125 0.081472658 0.18999724 ;
	setAttr ".tk[7]" -type "float3" -0.068660125 0.081472658 0.18999724 ;
	setAttr ".tk[8]" -type "float3" 0 0 -0.031548344 ;
	setAttr ".tk[9]" -type "float3" 0 0.052784927 -0.031548344 ;
	setAttr ".tk[10]" -type "float3" -0.043767523 0 -0.031548344 ;
	setAttr ".tk[11]" -type "float3" 0 -0.052784927 -0.031548344 ;
	setAttr ".tk[12]" -type "float3" 0.043767523 0 -0.031548344 ;
	setAttr ".tk[19]" -type "float3" 0 0.081472658 0.22317843 ;
	setAttr ".tk[22]" -type "float3" -0.077121906 0.081472658 0.081887133 ;
	setAttr ".tk[23]" -type "float3" 0.077121906 0.081472658 0.081887133 ;
	setAttr ".tk[26]" -type "float3" -0.068660125 0.081472658 0.19490846 ;
	setAttr ".tk[27]" -type "float3" -0.077121906 0.081472658 0.08679837 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "3120F81B-4630-C624-2552-C38B55087F4F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 1.0213810170784938 -3.7381072720150179 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.47257012 0.52138102 -4.0087729 ;
	setAttr ".rs" 54910;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.44514021277427673 0.52138101707849382 -4.2794388016323275 ;
	setAttr ".cbx" -type "double3" 0.5 0.52138101707849382 -3.7381072720150179 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "72028262-4EEC-5B3A-2195-DDBB543EB73B";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[0]" -type "float3" 0.085416473 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.085416473 0 0 ;
	setAttr ".tk[2]" -type "float3" 0.085416473 -0.19277835 -0.063097276 ;
	setAttr ".tk[3]" -type "float3" -0.085416473 -0.19277835 -0.063097276 ;
	setAttr ".tk[4]" -type "float3" 0.054859791 -0.19277835 0.034327805 ;
	setAttr ".tk[5]" -type "float3" -0.054859791 -0.19277835 0.034327805 ;
	setAttr ".tk[6]" -type "float3" 0.054859791 0 -0.041331518 ;
	setAttr ".tk[7]" -type "float3" -0.054859791 0 -0.041331518 ;
	setAttr ".tk[10]" -type "float3" -0.085416473 0 0 ;
	setAttr ".tk[12]" -type "float3" 0.085416473 0 0 ;
	setAttr ".tk[14]" -type "float3" 0 -0.19277835 0.0035273349 ;
	setAttr ".tk[15]" -type "float3" 0 -0.19277835 -0.10967951 ;
	setAttr ".tk[16]" -type "float3" 0 -0.19277835 0.0035273349 ;
	setAttr ".tk[17]" -type "float3" 0 0 -0.2074777 ;
	setAttr ".tk[18]" -type "float3" -0.054859791 0 -0.041331518 ;
	setAttr ".tk[19]" -type "float3" 0 0 -0.2074777 ;
	setAttr ".tk[20]" -type "float3" 0.054859791 0 -0.041331518 ;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "23E1367B-48F3-8A58-9B2A-EE890C5B4DC5";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyCube -n "polyCube1";
	rename -uid "70B71A9A-44B3-71A5-68EE-BEA0769796A9";
	setAttr ".cuv" 4;
createNode animCurveTL -n "pCylinder1_translateX";
	rename -uid "B27DF090-4FEA-5095-BBC5-5BB638470072";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pCylinder1_translateY";
	rename -uid "2E5A6993-4111-738F-CB45-089AEAA2121D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pCylinder1_translateZ";
	rename -uid "B48CBB98-445A-6675-148B-7B82E15A84B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pCylinder1_scaleX";
	rename -uid "9066ACA6-4159-7BA6-E067-A99B5F9EDF22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.39072998329605096;
createNode animCurveTU -n "pCylinder1_scaleY";
	rename -uid "4A5D545B-43D8-4786-C382-39A0ECABDF76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.46724021913698249;
createNode animCurveTU -n "pCylinder1_scaleZ";
	rename -uid "6874C255-48D1-0216-700B-47BE2C3BABD8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.39072998329605096;
createNode animCurveTU -n "pCylinder1_visibility";
	rename -uid "93C47EB2-4AE5-9793-B152-C09FA1B1E9A3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pCylinder1_rotateX";
	rename -uid "83309F56-47AC-1C72-9734-D8BDB9B8735D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder1_rotateY";
	rename -uid "CB34825E-4EAA-428B-4BDF-839818AC8CDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder1_rotateZ";
	rename -uid "71658428-49C7-DC44-0B64-5FAAFD687CDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "B9D0D26B-400D-D452-1415-AFB37BE791C7";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplit -n "polySplit9";
	rename -uid "FFF96C89-4261-B1F0-6198-8DAF7341CE2A";
	setAttr -s 19 ".e[0:18]"  0.57309902 0.57309902 0.42690101 0.57309902
		 0.42690101 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902
		 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902 0.57309902;
	setAttr -s 19 ".d[0:18]"  -2147483648 -2147483606 -2147483590 -2147483598 -2147483561 -2147483602 
		-2147483592 -2147483610 -2147483645 -2147483646 -2147483630 -2147483622 -2147483614 -2147483569 -2147483618 -2147483626 -2147483634 -2147483647 
		-2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit8";
	rename -uid "51449A57-4C56-2D99-5AAB-38A0C420CE21";
	setAttr -s 17 ".e[0:16]"  0.48831999 0.48831999 0.48831999 0.48831999
		 0.48831999 0.48831999 0.48831999 0.48831999 0.51168001 0.51168001 0.51168001 0.51168001
		 0.51168001 0.48831999 0.51168001 0.51168001 0.48831999;
	setAttr -s 17 ".d[0:16]"  -2147483642 -2147483629 -2147483621 -2147483613 -2147483616 -2147483624 
		-2147483632 -2147483641 -2147483637 -2147483608 -2147483591 -2147483600 -2147483597 -2147483589 -2147483605 -2147483638 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak26";
	rename -uid "547AF3DD-4078-E0B4-1583-FE8E289925DB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  0.060530275 0 0.027572371
		 -0.060530275 0 0.027572371 -0.060530275 0 -0.027572371 0.060530275 0 -0.027572371;
createNode polySplit -n "polySplit7";
	rename -uid "8CD7A500-4ED4-7EF4-2737-97ADDD15A53F";
	setAttr -s 5 ".e[0:4]"  0.500193 0.500193 0.500193 0.500193 0.500193;
	setAttr -s 5 ".d[0:4]"  -2147483604 -2147483603 -2147483601 -2147483599 -2147483604;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak25";
	rename -uid "7A2424AC-4B23-BE75-F197-28B1C3E3C70A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[24:27]" -type "float3"  0 -4.57089329 0 0 -4.57089329
		 0 0 -4.57089329 0 0 -4.57089329 0;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	rename -uid "575804CA-4E50-94CB-2FD0-8EA24915604E";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 4.3666437069669923 0 0 0 0 0.30456825922393932 0 0 0 0 8.4575883595983132 0
		 0 1.4781301848788144 -0.86968151148956474 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0925009 -0.84036237 ;
	setAttr ".rs" 57362;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.2160666361864618 1.0925009752374277 -1.9132582061956134 ;
	setAttr ".cbx" -type "double3" 1.2160666361864618 1.0925009752374277 0.23253343869274878 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak24";
	rename -uid "720A7AAE-4D7E-412F-12AA-62BF3E8D9760";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  0.26067269 -5.9775637e-08
		 0.40596446 -0.26067269 -5.9775637e-08 0.40596446 -0.26067269 -5.9775637e-08 -0.39903125
		 0.26067269 -5.9775637e-08 -0.39903125;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	rename -uid "F680C55E-418F-71FB-FA96-F399973ACBA4";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 4.3666437069669923 0 0 0 0 0.30456825922393932 0 0 0 0 8.4575883595983132 0
		 0 1.4781301848788144 -0.86968151148956474 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.0925009 -0.86968178 ;
	setAttr ".rs" 37285;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.3543311306618784 1.0925009752374277 -5.3467390350145152 ;
	setAttr ".cbx" -type "double3" 2.3543311306618784 1.0925009752374277 3.6073755079238361 ;
	setAttr ".raf" no;
createNode polyTweak -n "polyTweak23";
	rename -uid "38616236-44B5-EE5F-BB6F-2BAF56204172";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[0:19]" -type "float3"  -0.039162606 1.24305594 0.029353878
		 0.039162606 1.24305594 0.029353878 -0.039162606 -0.47420353 0.029353878 0.039162606
		 -0.47420353 0.029353878 -0.039162606 -0.47420353 -0.029353878 0.039162606 -0.47420353
		 -0.029353878 -0.039162606 1.24305594 -0.029353878 0.039162606 1.24305594 -0.029353878
		 0 -6.3329935e-08 0 0 -6.3329935e-08 0 0 -6.3329935e-08 0 0 -6.3329935e-08 0 0 -6.3329935e-08
		 0 0 -6.3329935e-08 0 0 -6.3329935e-08 0 0 -6.3329935e-08 0 0 -0.55553722 0 0 -0.55553722
		 0 0 -0.55553722 0 0 -0.55553722 0;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	rename -uid "782181DE-4385-1CC2-9A04-3F88FAB16E86";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 4.3666437069669923 0 0 0 0 0.30456825922393932 0 0 0 0 10.281500686221438 0
		 0 1.4781301848788144 -0.57699336904357545 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.6304145 -0.5769937 ;
	setAttr ".rs" 63689;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.9833218917991609 1.6304144415665645 -5.5177445324362475 ;
	setAttr ".cbx" -type "double3" 1.9833218917991609 1.6304144415665645 4.3637571815239014 ;
	setAttr ".raf" no;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	rename -uid "E439AD45-4A9A-467F-A19E-B8A04621969A";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 4.3666437069669923 0 0 0 0 0.30456825922393932 0 0 0 0 10.281500686221438 0
		 0 1.4781301848788144 -0.57699336904357545 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.6304145 -0.5769937 ;
	setAttr ".rs" 49059;
	setAttr ".off" 0.20000000298023224;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.1833218534834962 1.6304144415665645 -5.7177443249794901 ;
	setAttr ".cbx" -type "double3" 2.1833218534834962 1.6304144415665645 4.563756974067144 ;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "7B461134-4921-7421-8A49-12AF3D380407";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 4.3666437069669923 0 0 0 0 0.30456825922393932 0 0 0 0 10.281500686221438 0
		 0 1.4781301848788144 -0.57699336904357545 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 1.6304144 -0.57699335 ;
	setAttr ".rs" 39314;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.1833218534834962 1.6304143871055157 -5.7177437121542942 ;
	setAttr ".cbx" -type "double3" 2.1833218534834962 1.6304143871055157 4.563756974067144 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "280AE6FA-4E98-0B0D-3124-06ACE85BF668";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[0:11]" -type "float3"  0 -2.009206295 0 0 -2.009206295
		 0 -1.8626451e-08 2.3841858e-07 1.8626451e-08 1.8626451e-08 2.3841858e-07 1.8626451e-08
		 -1.8626451e-08 2.3841858e-07 -1.4901161e-08 1.8626451e-08 2.3841858e-07 -1.4901161e-08
		 0 -2.009206295 0 0 -2.009206295 0 -1.8626451e-08 2.3841858e-07 1.8626451e-08 1.8626451e-08
		 2.3841858e-07 1.8626451e-08 1.8626451e-08 2.3841858e-07 -1.4901161e-08 -1.8626451e-08
		 2.3841858e-07 -1.4901161e-08;
createNode polyCube -n "polyCube2";
	rename -uid "5983362A-4845-9356-B91D-7D90502D241C";
	setAttr ".cuv" 4;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyExtrudeFace17.out" "pCubeShape1.i";
connectAttr "polySplit9.out" "pCubeShape2.i";
connectAttr "pCylinder1_translateX.o" "pCylinder1.tx";
connectAttr "pCylinder1_translateY.o" "pCylinder1.ty";
connectAttr "pCylinder1_translateZ.o" "pCylinder1.tz";
connectAttr "pCylinder1_scaleX.o" "pCylinder1.sx";
connectAttr "pCylinder1_scaleY.o" "pCylinder1.sy";
connectAttr "pCylinder1_scaleZ.o" "pCylinder1.sz";
connectAttr "pCylinder1_visibility.o" "pCylinder1.v";
connectAttr "pCylinder1_rotateX.o" "pCylinder1.rx";
connectAttr "pCylinder1_rotateY.o" "pCylinder1.ry";
connectAttr "pCylinder1_rotateZ.o" "pCylinder1.rz";
connectAttr "polyCylinder1.out" "pCylinderShape1.i";
connectAttr "polyBevel3.out" "polyExtrudeFace17.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace17.mp";
connectAttr "polyTweak27.out" "polyBevel3.ip";
connectAttr "pCubeShape1.wm" "polyBevel3.mp";
connectAttr "polySmoothFace1.out" "polyTweak27.ip";
connectAttr "polyTweak21.out" "polySmoothFace1.ip";
connectAttr "polyExtrudeFace11.out" "polyTweak21.ip";
connectAttr "polyTweak20.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace11.mp";
connectAttr "polySplit6.out" "polyTweak20.ip";
connectAttr "polyTweak19.out" "polySplit6.ip";
connectAttr "polyExtrudeFace10.out" "polyTweak19.ip";
connectAttr "polyTweak18.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak18.ip";
connectAttr "polyTweak17.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak17.ip";
connectAttr "polyTweak16.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak16.ip";
connectAttr "polyTweak15.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak15.ip";
connectAttr "polyTweak14.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polySplit5.out" "polyTweak14.ip";
connectAttr "polyTweak13.out" "polySplit5.ip";
connectAttr "polySplit4.out" "polyTweak13.ip";
connectAttr "polyTweak12.out" "polySplit4.ip";
connectAttr "polyBevel2.out" "polyTweak12.ip";
connectAttr "polyTweak11.out" "polyBevel2.ip";
connectAttr "pCubeShape1.wm" "polyBevel2.mp";
connectAttr "polyBevel1.out" "polyTweak11.ip";
connectAttr "polyTweak10.out" "polyBevel1.ip";
connectAttr "pCubeShape1.wm" "polyBevel1.mp";
connectAttr "polySplit3.out" "polyTweak10.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polyTweak9.out" "polySplit2.ip";
connectAttr "polyExtrudeFace5.out" "polyTweak9.ip";
connectAttr "polyTweak8.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak8.ip";
connectAttr "polyTweak7.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak7.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polySplit1.out" "polyTweak6.ip";
connectAttr "polyTweak5.out" "polySplit1.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak5.ip";
connectAttr "polyMergeVert2.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak4.out" "polyMergeVert2.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak4.ip";
connectAttr "polyTweak3.out" "polyMergeVert1.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert1.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak3.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeEdge1.out" "polyTweak2.ip";
connectAttr "polyTweak1.out" "polyExtrudeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge1.mp";
connectAttr "polySubdFace1.out" "polyTweak1.ip";
connectAttr "polyCube1.out" "polySubdFace1.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "polyTweak26.out" "polySplit8.ip";
connectAttr "polySplit7.out" "polyTweak26.ip";
connectAttr "polyTweak25.out" "polySplit7.ip";
connectAttr "polyExtrudeFace16.out" "polyTweak25.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace16.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace16.mp";
connectAttr "polyExtrudeFace15.out" "polyTweak24.ip";
connectAttr "polyTweak23.out" "polyExtrudeFace15.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace15.mp";
connectAttr "polyExtrudeFace14.out" "polyTweak23.ip";
connectAttr "polyExtrudeFace13.out" "polyExtrudeFace14.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace12.out" "polyExtrudeFace13.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace13.mp";
connectAttr "polyTweak22.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape2.wm" "polyExtrudeFace12.mp";
connectAttr "polyCube2.out" "polyTweak22.ip";
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
// End of bodyman.ma
