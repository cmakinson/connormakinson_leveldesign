﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bookcasekey : MonoBehaviour
{

    public GameObject BOOKCASEDOOR;
    public KeyPickUp_Com key;
    public MouseListener_Com mouseListen;
    public float targetRot = 90f;
    GameObject myPlayer;
    bool doorOpen;
    bool inTrigger;
    bool clickHappened = false;
    public float moveSpeed = 1f;
    public float snapTo = 0.1f;
    public Image cursorImage;

    //public static bool hasKey;

    Vector3 closedRot;
    bool doorUnlocked = true;

    // Use this for initialization
    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
       
    }

    void Update()
    {
        if (inTrigger)
        {
            //turn on cursor when enters switch trigger with cursor over switch  
            if (mouseListen.mouseCursorOn == true)
            {
                if (cursorImage.enabled != true)
                {
                    cursorImage.enabled = true;
                }
            }
            //turn off cursor when player leaves trigger with cursor over switch  
            else
            {
                cursorImage.enabled = false;
            }
        }

        if (inTrigger && mouseListen.mouseClicked && !clickHappened)
        {
            clickHappened = true;
            if (key == null)
            {
                DoorInteract();
            }
            else if (key.keyPickedUp == true)
            {
                if (doorUnlocked)
                {
                    doorUnlocked = false;
                }
                DoorInteract();
            }
            else
            {
                Locked();
            }
        }
        else if (!mouseListen.mouseClicked)
        {
            clickHappened = false;
        }
    }

    void Locked()
    {
        Debug.Log("Door Locked");
        CancelInvoke();
    }

    //Works out which way door should open and starts coroutine
    void DoorInteract()
    {
        Vector3 finishPos;
        if (!doorOpen)
        {
            Debug.Log("doors");
            Vector3 playerDir = BOOKCASEDOOR.transform.position - myPlayer.transform.position;
            Debug.Log("dot");
            doorOpen = true;
        }
        else
        {
            finishPos = closedRot;
            doorOpen = false;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = false;
            inTrigger = false;
        }
    }
}

