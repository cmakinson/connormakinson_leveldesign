﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Letter01 : MonoBehaviour
{
    public GameObject letter01;
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Image day1message;
    public bool LetterOpen = false;

    // Use this for initialization
    void Start () {
        cursorImage.enabled = false;

        if(LetterOpen == true)
        {
            day1message.enabled = true;
        }
        else { day1message.enabled = false; }
    }
    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseDown()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (day1message.enabled != true)
            {
               day1message.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            day1message.enabled = false;
        }
    }

    void OnMouseExit()
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            day1message.enabled = false;
        }
    }	
}
