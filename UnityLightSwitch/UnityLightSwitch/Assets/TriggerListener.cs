﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//These are name spaces: Locations of code that we need. 

//This is a class that inherits from monobehaviour.
public class TriggerListener : MonoBehaviour
{
    //At the top of classes we declare our variables.
    public bool PlayerEntered = false;
	// Use this for initialization
	void Start ()
    {
        Debug.Log("Start was called");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was called");
    }
    //Checks if player enters trigger.
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerEntered = true;
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerEntered = false;
        }
    }
}
