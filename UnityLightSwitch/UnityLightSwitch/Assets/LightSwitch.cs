﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener Trigger;
    public Image CursorImage;

    //Light variable
    public Light SpotLight;
    //audio variable
    public AudioSource SwitchAudio;
    //Animation variable
    Animation anim;

	// Use this for initialization
	void Start ()
    {
        SwitchAudio = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        CursorImage.enabled = false; 
	}
	
	// called when cursor is over object
	void OnMouseOver ()
    {
        if (Trigger.PlayerEntered == true)
        {
            if (CursorImage.enabled != true)
            {
                CursorImage.enabled = true;
            }
            Debug.Log("Mouse over switch");
        }
        else
        {
            CursorImage.enabled = false;
        }
	}

    void OnMouseExit()
    {
        if (CursorImage.enabled == true)
        {
            CursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (Trigger.PlayerEntered == true)
        {
            SwitchAudio.Play();
            anim.Stop();
            anim.Play();

            if (SpotLight.intensity > 0f)
            {
                SpotLight.intensity = 0f;
            }
            else
            {
                SpotLight.intensity = 1.5f;
            }

        } 
    }
}
